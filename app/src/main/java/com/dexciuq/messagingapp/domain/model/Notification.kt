package com.dexciuq.messagingapp.domain.model

data class Notification(
    val title: String,
    val message: String,
    val icon: Int = 0,
    val id: Long = 0,
)