package com.dexciuq.messagingapp.domain.repository

import androidx.lifecycle.LiveData
import com.dexciuq.messagingapp.domain.model.Notification

interface NotificationRepository {
    fun getNotifications(): LiveData<List<Notification>>
}