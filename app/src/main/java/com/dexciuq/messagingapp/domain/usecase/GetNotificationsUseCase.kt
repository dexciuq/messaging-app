package com.dexciuq.messagingapp.domain.usecase

import androidx.lifecycle.LiveData
import com.dexciuq.messagingapp.domain.model.Notification
import com.dexciuq.messagingapp.domain.repository.NotificationRepository
import javax.inject.Inject

class GetNotificationsUseCase @Inject constructor (
    private val repository: NotificationRepository
) {
    operator fun invoke(): LiveData<List<Notification>> = repository.getNotifications()
}