package com.dexciuq.messagingapp.presentation.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.dexciuq.messagingapp.R
import com.dexciuq.messagingapp.data.db.AppDatabase
import com.dexciuq.messagingapp.data.model.NotificationEntity
import com.dexciuq.messagingapp.presentation.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private const val CHANNEL_ID = "PushNotificationChannelId"
private const val CHANNEL_NAME = "PushNotificationChannel"
private const val REGULAR = "regular"
private const val URGENT = "urgent"
private const val TAG = "PushNotificationService"

class PushNotificationService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.i(TAG, "onNewToken: $token}")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
        )

        Log.i(TAG, "onMessageReceived: ${message.data}")

        val notification = when(message.data["type"]) {
            URGENT -> createUrgentNotification(
                title = message.notification!!.title!!,
                message = """
                    Body: ${message.notification!!.body!!}
                    Sender: ${message.data["sender"]!!}
                    Expired at: ${message.data["expiration_date"]!!}
                """.trimIndent(),
                icon = R.drawable.ic_notification,
                pendingIntent = pendingIntent,
            )
            REGULAR -> createRegularNotification(
                title = message.notification!!.title!!,
                message = """
                    Body: ${message.notification!!.body!!}
                    Sender: ${message.data["sender"]!!}
                """.trimIndent(),
                icon = R.drawable.ic_sync,
                pendingIntent = pendingIntent,
            )
            else -> error("Unexpected type")
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(0, notification)
    }

    private fun createRegularNotification(
        title: String, message: String, icon: Int, pendingIntent: PendingIntent
    ): Notification {
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(message)
            .setColor(Color.BLUE)
            .setSmallIcon(icon)
            .setContentIntent(pendingIntent)
            .setSound(null)
            .setAutoCancel(true)
            .build()

        saveNotification(title, message, icon)
        return notification
    }

    private fun createUrgentNotification(
        title: String, message: String, icon: Int, pendingIntent: PendingIntent
    ): Notification {
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(icon)
            .setColor(Color.RED)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        saveNotification(title, message, icon)
        return notification
    }

    private fun saveNotification(title: String, message: String, icon: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            AppDatabase.getInstance(this@PushNotificationService)
                .notificationDao()
                .insert(
                    NotificationEntity(title, message, icon)
                )
        }
    }
}