package com.dexciuq.messagingapp.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dexciuq.messagingapp.domain.model.Notification
import com.dexciuq.messagingapp.domain.usecase.GetNotificationsUseCase

class MainViewModel(
    getNotificationsUseCase: GetNotificationsUseCase,
) : ViewModel() {

    val notifications: LiveData<List<Notification>> = getNotificationsUseCase()
}