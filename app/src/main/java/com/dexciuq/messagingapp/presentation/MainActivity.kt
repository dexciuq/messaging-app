package com.dexciuq.messagingapp.presentation

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.dexciuq.messagingapp.databinding.ActivityMainBinding
import com.dexciuq.messagingapp.di.AppModule
import com.dexciuq.messagingapp.di.DaggerAppComponent
import com.dexciuq.messagingapp.presentation.adapter.NotificationAdapter
import com.google.firebase.messaging.FirebaseMessaging
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerAppComponent.builder()
            .appModule(AppModule(application))
            .build()
            .inject(this)

        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            Log.d("MainActivity", "Retrieve token: $it")
        }

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        requestNotificationPermission()

        val adapter = NotificationAdapter()
        binding.recyclerView.adapter = adapter
        viewModel.notifications.observe(this, adapter::submitList)
    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission(),
    ) { isGranted: Boolean ->
        if (isGranted) {
            // Nothing
        } else {
            Toast.makeText(
                this@MainActivity,
                "App requires to allow notifications",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun requestNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.POST_NOTIFICATIONS
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                // Nothing
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
                showPermissionExplanationDialog()
            } else {
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun showPermissionExplanationDialog() {
        AlertDialog.Builder(this).apply {
            setTitle("Notification Permission")
            setMessage("Granting notification permission allows us to send you notifications.")
            setPositiveButton("OK") { _, _ ->
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
            setNegativeButton("No, thanks") { it, _ -> it.dismiss() }
            show()
        }
    }
}