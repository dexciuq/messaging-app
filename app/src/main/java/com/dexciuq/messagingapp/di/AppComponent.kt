package com.dexciuq.messagingapp.di

import com.dexciuq.messagingapp.presentation.MainActivity
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}