package com.dexciuq.messagingapp.di

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.dexciuq.messagingapp.data.datasource.LocalDBDataSource
import com.dexciuq.messagingapp.data.datasource.NotificationDataSource
import com.dexciuq.messagingapp.data.db.AppDatabase
import com.dexciuq.messagingapp.data.db.dao.NotificationDao
import com.dexciuq.messagingapp.data.mapper.NotificationMapper
import com.dexciuq.messagingapp.data.repository.NotificationRepositoryImpl
import com.dexciuq.messagingapp.domain.repository.NotificationRepository
import com.dexciuq.messagingapp.domain.usecase.GetNotificationsUseCase
import com.dexciuq.messagingapp.presentation.MainViewModel
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: Application) {

    @Provides
    fun provideAppDatabase(): AppDatabase {
        return AppDatabase.getInstance(application)
    }

    @Provides
    fun provideNotificationDao(database: AppDatabase): NotificationDao {
        return database.notificationDao()
    }

    @Provides
    fun provideNotificationDataSource(
        mapper: NotificationMapper,
        notificationDao: NotificationDao,
    ): NotificationDataSource {
        return LocalDBDataSource(mapper, notificationDao)
    }

    @Provides
    fun provideNotificationRepository(dataSource: NotificationDataSource): NotificationRepository {
        return NotificationRepositoryImpl(dataSource)
    }

    @Provides
    fun provideMainViewModelFactory(
        getNotificationsUseCase: GetNotificationsUseCase
    ): ViewModelProvider.Factory {
        return viewModelFactory {
            initializer {
                MainViewModel(getNotificationsUseCase)
            }
        }
    }
}