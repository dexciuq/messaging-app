package com.dexciuq.messagingapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dexciuq.messagingapp.data.model.NotificationEntity

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notifications")
    fun getAll(): LiveData<List<NotificationEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(notificationEntity: NotificationEntity)
}