package com.dexciuq.messagingapp.data.datasource

import androidx.lifecycle.LiveData
import com.dexciuq.messagingapp.domain.model.Notification

interface NotificationDataSource {
    fun getNotifications(): LiveData<List<Notification>>
}