package com.dexciuq.messagingapp.data.mapper

import com.dexciuq.messagingapp.data.model.NotificationEntity
import com.dexciuq.messagingapp.domain.model.Notification
import javax.inject.Inject

class NotificationMapper @Inject constructor() {
    fun toDomain(notificationEntity: NotificationEntity) = Notification(
        title = notificationEntity.title,
        message = notificationEntity.message,
        icon = notificationEntity.icon,
        id = notificationEntity.id,
    )
}