package com.dexciuq.messagingapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dexciuq.messagingapp.data.db.dao.NotificationDao
import com.dexciuq.messagingapp.data.model.NotificationEntity

@Database(entities = [NotificationEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun notificationDao(): NotificationDao

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context) = instance ?: synchronized(AppDatabase::class) {
            Room.databaseBuilder(
                context = context.applicationContext,
                klass = AppDatabase::class.java,
                name = "notification.db"
            ).build().also { instance = it }
        }
    }
}