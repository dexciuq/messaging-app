package com.dexciuq.messagingapp.data.datasource

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.dexciuq.messagingapp.data.db.dao.NotificationDao
import com.dexciuq.messagingapp.data.mapper.NotificationMapper
import com.dexciuq.messagingapp.domain.model.Notification

class LocalDBDataSource(
    private val mapper: NotificationMapper,
    private val notificationDao: NotificationDao,
) : NotificationDataSource {
    override fun getNotifications(): LiveData<List<Notification>> =
        notificationDao.getAll().map {
            it.map(mapper::toDomain)
        }
}