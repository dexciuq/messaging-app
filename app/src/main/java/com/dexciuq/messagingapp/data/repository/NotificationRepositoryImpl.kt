package com.dexciuq.messagingapp.data.repository

import androidx.lifecycle.LiveData
import com.dexciuq.messagingapp.data.datasource.NotificationDataSource
import com.dexciuq.messagingapp.domain.model.Notification
import com.dexciuq.messagingapp.domain.repository.NotificationRepository

class NotificationRepositoryImpl(
    private val dataSource: NotificationDataSource,
) : NotificationRepository {
    override fun getNotifications(): LiveData<List<Notification>> = dataSource.getNotifications()
}