package com.dexciuq.messagingapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dexciuq.messagingapp.R

@Entity(tableName = "notifications")
data class NotificationEntity(
    val title: String,
    val message: String,
    val icon: Int = R.drawable.ic_notification,
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
)
